package br.com.numerosromanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTeste
{
    @Test
    public void TestarConversorParaRomanos0()
    {
        //Preparação
        int numeroAConverter = 0;

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () ->{Conversor.converterParaAlgarismosRomanos(numeroAConverter);});
    }

    @Test
    public void TestarConversorParaRomanos1()
    {
        //Preparação
        int numeroAConverter = 1;
        String numeroConvertidoEsperado = "I";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos2()
    {
        //Preparação
        int numeroAConverter = 2;
        String numeroConvertidoEsperado = "II";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos3()
    {
        //Preparação
        int numeroAConverter = 3;
        String numeroConvertidoEsperado = "III";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos4()
    {
        //Preparação
        int numeroAConverter = 4;
        String numeroConvertidoEsperado = "IV";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos5()
    {
        //Preparação
        int numeroAConverter = 5;
        String numeroConvertidoEsperado = "V";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos6()
    {
        //Preparação
        int numeroAConverter = 6;
        String numeroConvertidoEsperado = "VI";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos7()
    {
        //Preparação
        int numeroAConverter = 7;
        String numeroConvertidoEsperado = "VII";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos8()
    {
        //Preparação
        int numeroAConverter = 8;
        String numeroConvertidoEsperado = "VIII";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos9()
    {
        //Preparação
        int numeroAConverter = 9;
        String numeroConvertidoEsperado = "IX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos10()
    {
        //Preparação
        int numeroAConverter = 10;
        String numeroConvertidoEsperado = "X";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos20()
    {
        //Preparação
        int numeroAConverter = 20;
        String numeroConvertidoEsperado = "XX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos30()
    {
        //Preparação
        int numeroAConverter = 30;
        String numeroConvertidoEsperado = "XXX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos40()
    {
        //Preparação
        int numeroAConverter = 40;
        String numeroConvertidoEsperado = "XL";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos50()
    {
        //Preparação
        int numeroAConverter = 50;
        String numeroConvertidoEsperado = "L";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos60()
    {
        //Preparação
        int numeroAConverter = 60;
        String numeroConvertidoEsperado = "LX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos70()
    {
        //Preparação
        int numeroAConverter = 70;
        String numeroConvertidoEsperado = "LXX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos80()
    {
        //Preparação
        int numeroAConverter = 80;
        String numeroConvertidoEsperado = "LXXX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos90()
    {
        //Preparação
        int numeroAConverter = 90;
        String numeroConvertidoEsperado = "XC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos100()
    {
        //Preparação
        int numeroAConverter = 100;
        String numeroConvertidoEsperado = "C";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos200()
    {
        //Preparação
        int numeroAConverter = 200;
        String numeroConvertidoEsperado = "CC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos300()
    {
        //Preparação
        int numeroAConverter = 300;
        String numeroConvertidoEsperado = "CCC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos400()
    {
        //Preparação
        int numeroAConverter = 400;
        String numeroConvertidoEsperado = "CD";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos500()
    {
        //Preparação
        int numeroAConverter = 500;
        String numeroConvertidoEsperado = "D";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos600()
    {
        //Preparação
        int numeroAConverter = 600;
        String numeroConvertidoEsperado = "DC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos700()
    {
        //Preparação
        int numeroAConverter = 700;
        String numeroConvertidoEsperado = "DCC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos800()
    {
        //Preparação
        int numeroAConverter = 800;
        String numeroConvertidoEsperado = "DCCC";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos900()
    {
        //Preparação
        int numeroAConverter = 900;
        String numeroConvertidoEsperado = "CM";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos1000()
    {
        //Preparação
        int numeroAConverter = 1000;
        String numeroConvertidoEsperado = "M";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos19()
    {
        //Preparação
        int numeroAConverter = 19;
        String numeroConvertidoEsperado = "XIX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos84()
    {
        //Preparação
        int numeroAConverter = 84;
        String numeroConvertidoEsperado = "LXXXIV";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos835()
    {
        //Preparação
        int numeroAConverter = 835;
        String numeroConvertidoEsperado = "DCCCXXXV";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos997()
    {
        //Preparação
        int numeroAConverter = 997;
        String numeroConvertidoEsperado = "CMXCVII";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos1849()
    {
        //Preparação
        int numeroAConverter = 1849;
        String numeroConvertidoEsperado = "MDCCCXLIX";

        //Execução
        String numeroConvertido = Conversor.converterParaAlgarismosRomanos(numeroAConverter);

        //Verificação
        Assertions.assertEquals(numeroConvertidoEsperado, numeroConvertido);
    }

    @Test
    public void TestarConversorParaRomanos4000()
    {
        //Preparação
        int numeroAConverter = 4000;

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () ->{Conversor.converterParaAlgarismosRomanos(numeroAConverter);});
    }
}

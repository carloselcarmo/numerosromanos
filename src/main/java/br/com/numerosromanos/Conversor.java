package br.com.numerosromanos;

import java.util.List;

public class Conversor
{
    public static String converterParaAlgarismosRomanos(Integer numeroAConverter)
    {
        String numeroRomano = "";

        if(validarNumeroAConverter(numeroAConverter))
        {
            for (NumeroRomano numero : NumeroRomano.values())
            {
                Integer resultado = numeroAConverter / numero.getValor();

                if (resultado > 0)
                {
                    for (int i = 0; i < resultado; i++)
                    {
                        numeroRomano += numero.name();
                    }
                }
                numeroAConverter = numeroAConverter - (numero.getValor() * resultado);
            }
        }
        return numeroRomano;
    }

    private static boolean validarNumeroAConverter(Integer numeroAConverter)
    {
        if(numeroAConverter == 0)
        {
            throw new RuntimeException("Não existe o número 0 em algarismos romanos");
        }
        else if( numeroAConverter >= 4000)
        {
            throw new RuntimeException("O conversor funciona para números até 3999");
        }
        return true;
    }
}
